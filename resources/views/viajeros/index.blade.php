@extends('layouts.app')

@section('content')

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><b> <h1 class="text-primary">CRUD de Viajero</h1></b></center></div>

                <div class="card-body">

                    <center>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="limpiar()"><i class="fa fa-plus"></i> Nuevo Viajero</button>
                    </center>
                <hr>
                <!--Mensajes de alertas-->
                <div class="alert alert-success" role="alert"  id="success" style="display: none">
                 <strong><i class="fa fa-check-circle"> </i> Exito !!</strong>  <a href="#" class="alert-link">V</a>.
               </div>
                
                



                <table class="table table-striped" id="tabviajes">
                    <thead>
                        <tr>
                        
                            <th>Cédula</th>
                            <th>Nombre</th>
                            <th>Dirección</th>
                            <th>Telefono</th>
                            <th></th>
                        </tr>
                    </thead>
    

                    <tbody>
                        @foreach($viajeros as $viajero)
                        <tr id="tab{{ $viajero->id }}">
                            <td>{{ $viajero->cedula }}</td>
                            <td>{{ $viajero->nombre }}</td>
                            <td>{{ $viajero->direccion }}</td>
                            <td>{{ $viajero->phone }}</td>
                            <td>
                              

                               <center>
                            <button class="btn btn-success btn-sm"  title="Ver Información"  onclick="show('{{ $viajero->id }}','{{ $viajero->cedula }}','{{ $viajero->nombre }}','{{ $viajero->direccion }}','{{ $viajero->phone }}')"><i class="fa fa-search"></i></button>
                            <button class="btn btn-primary btn-sm" title="Editar Viajero" onclick="llenar('{{ $viajero->id }}','{{ $viajero->cedula }}','{{ $viajero->nombre }}','{{ $viajero->direccion }}','{{ $viajero->phone }}')"><i class="fa fa-edit"></i></button>
                            <button class="btn btn-danger btn-sm" title="Anular Viajero" onclick="deleteV('{{ $viajero->id }}','{{ $viajero->cedula }}','{{ $viajero->nombre }}','{{ $viajero->direccion }}','{{ $viajero->phone }}')"><i class="fa fa-trash"></i></button>

                        </center>
                            </td>
                          
                        </tr>
                        @endforeach
                    </tbody>

                   
                </table>

                <hr>
                <center>

                {{ $viajeros->links()}}
                   </center>
                </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

<!--Inclusion de modal Para Nuevo Viaje-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ingreso | Edición Viajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="alert alert-danger" role="alert" id="erros" style="display: none">
                 <strong><i class="fa fa-info-circle"> </i> Error !!</strong>  <a href="#" class="alert-link"></a>.
               </div>

        <form  >
  <div class="form-group">
    <label for="exampleInputEmail1">Cédula Viajero</label>
    <input type="text" id="cedula" class="form-control " >
    <input type="hidden" id="id_viajero" >
  
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Nombre Y Apellidos </label>
    <input type="text" id="nombres" class="form-control text-uppercase" >
   
  </div>

   <div class="form-group">
    <label for="exampleInputPassword1">Dirección Fisica </label>
    <textarea  id="direccion" class="form-control text-uppercase"></textarea>
   
    
  </div>

   <div class="form-group">
    <label for="exampleInputPassword1">Teléfono Celular </label>
    <input type="text" id="phone" class="form-control" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </div>

 

</form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_new" class="btn btn-primary" onclick="newV()"><i class="fa fa-check-circle"></i> Guardar </button>
          <button type="button" id="btn_edit" class="btn btn-primary" onclick="UpdateV()"><i class="fa fa-pencil"></i> Guardar Cambios </button>
      
      </div>
    </div>
  </div>
</div>

<!--Ver detalle de viaje-->

<div class="modal fade" id="showViaje" tabindex="-1" role="dialog" aria-labelledby="showViaje" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="showViaje">Detalle De Viaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="Mdet">
          
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    
      
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript">
 
//establecemos permisos de seguridad para el enlace con ajax y controlador 

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});


//Limpar Formulario
    function limpiar(){

   $('#cedula').val("");   
   $('#nombres').val("");
   $('#direccion').val("");   
   $('#phone').val("");

//inabilitar botones a la hora de nuevo registro
   $('#btn_edit').hide();
   $('#btn_new').show();

             
    }

     //funcion para crear un nuevo viajero 
   function newV(){

  $.ajax({
   url:'viajeros'
  ,type:'post'
  ,data:{//data relacionada a formulario de registro
  '_token': $('input[name=_token]').val(),
  'cedula':$('#cedula').val(),
  'nombres':$('#nombres').val(),
  'direccion':$('#direccion').val(),
  'telefono':$('#phone').val(),
  

}
  ,success: function(data){

    if ((data.status===500)) {
            //verificamos que campos seran validados
             errorsHtml="";
           $.each(data.errors, function (key, value) {
             errorsHtml += value +"\n";
          });

              //validamos que no sea un error en bases de datos
       $("#erros").show();
          $('.alert-link').text(errorsHtml);
           setTimeout(function() { $("#erros").hide(); },8000);

           
    }else if((data.status===404)) {

          $("#erros").show();
          $('.alert-link').text(data.errors);
           setTimeout(function() { $("#erros").hide(); },8000);



    }else{
         //limpiamos en formulario y ocultamos modals
            limpiar();/*Limpiamos formulario*/           
        
         $('#exampleModal').modal('hide');  //ocultamos formulario  
          setTimeout(function() { $("#success").fadeIn(1500);},3000);//activamos Notificación
          $('.alert-link').text("La Creacion Del Viajero Se Ejecuto Satisfactoriamente");//mensaje de notificación
           setTimeout(function() { $("#success").fadeOut(1500); },6000);//desactivamos notidicación

           setTimeout("location.href='viajeros'",4000);  
        
    }
    
    }
  ,error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.responseText);
        alert(thrownError);
  }
  });
   
   }


   //mostrar detalle del viaje
function show(id,cedula,nombre,direccion,phone){

$('#Mdet').html('<table class="table table-striped"><tr><th>ID:<th><td>'+id+'</td></tr><tr><th>Cédula:<th><td>'+cedula+'</td></tr><tr><th>Nombre y Apellido:<th><td>'+nombre+'</td></tr><tr><th>Direccion:<th><td>'+direccion+'</td></tr><tr><th>Telefono:<th><td>'+phone+'</td></table>');
$('#showViaje').modal('show');
}

//Cargar Formulario para editar //
function llenar(id,cedula,nombre,direccion,phone){
   $('#exampleModal').modal('show');
   $('#id_viajero').val(id);
   
   $('#cedula').val(cedula);   
   $('#nombres').val(nombre);
   $('#direccion').val(direccion);   
   $('#phone').val(phone);


   //habilito boton guardar cambios
   $('#btn_edit').show();
   $('#btn_new').hide();


}


//editar viajero
   function UpdateV(){
    var id=$('#id_viajero').val()
  $.ajax({
   url:'viajeros/'+id
  ,type:'PUT'
  ,data:{//data relacionada a formulario de registro
  '_token': $('input[name=_token]').val(),
  'cedula':$('#cedula').val(),
  'nombres':$('#nombres').val(),
  'direccion':$('#direccion').val(),
  'telefono':$('#phone').val(),

}
  ,success: function(data){

     if ((data.status===500)) {

          //verificamos que campos seran validados
             errorsHtml="";
           $.each(data.errors, function (key, value) {
             errorsHtml += value +"\n";
          });

              //validamos que no sea un error en bases de datos
       $("#erros").show();
          $('.alert-link').text(errorsHtml);
           setTimeout(function() { $("#erros").hide(); },8000);
    }else{

            //limpiamos en formulario y ocultamos modals
            limpiar();/*Limpiamos formulario*/           
        
         $('#exampleModal').modal('hide');  //ocultamos formulario
          setTimeout(function() { $("#success").fadeIn(1500);},3000);//activamos Notificción
          $('.alert-link').text("La Modificación Del Viajero Se Ejecuto Satisfactoriamente");//mensaje de notificación

           setTimeout(function() { $("#success").fadeOut(1500); },6000);//desactivamos notificación
           setTimeout("location.href='viajeros'",4000);  

    }
    
    }
  ,error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.responseText);
        alert(thrownError);
  }
  });
   
   }

//metodo para eliminar Viajeros

function deleteV(id,cedula,nombre,direccion,phone){

    //mensaje de validación de accion delete//
    if(confirm("Seguro que desea Eliminar al Viajero "+nombre+"?")){

    $.ajax({
                type: 'DELETE',//metoodo 
                url: 'viajeros/' + id,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {

                  
                   setTimeout("location.href='viajeros'",1500);               
                }
            });
}
}

</script>



