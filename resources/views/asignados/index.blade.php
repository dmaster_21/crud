@extends('layouts.app')

@section('content')

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><b> <h1 class="text-primary">CRUD de Viajes y Vajeros</h1></b></center></div>

                <div class="card-body">

                    <center>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="limpiar()"><i class="fa fa-plus"></i> Asignar</button>
                    </center>
                <hr>
                <!--Mensajes de alertas-->
                <div class="alert alert-success" role="alert"  id="success" style="display: none">
                 <strong><i class="fa fa-check-circle"> </i> Exito !!</strong>  <a href="#" class="alert-link">V</a>.
               </div>
                
                



                <table class="table table-striped" id="tabviajes">
                    <thead>
                        <tr>
                            <th>Cédula </th>
                            <th>Nombre </th>
                            <th>Código de Viaje</th>
                            <th>Origen </th>
                            <th>Destino</th>
                            <th>Costo</th>
                            <th></th>
                        </tr>
                    </thead>
    

                    <tbody>
                        @foreach($asignados as $asignado)
                        <tr id="tab{{ $asignado->id }}">
                            <td>{{ $asignado->cedula }}</td>
                            <td>{{ $asignado->nombre }}</td>
                            <td>{{ $asignado->cdg_viaje }}</td>
                            <td>{{ $asignado->dsc_origen }}</td>
                            <td>{{ $asignado->dsc_destino }}</td>
                            <td>{{ $asignado->costo_viaje }}</td>
                            <td>
                              
                                             <center>
                            <button class="btn btn-success btn-sm"  title="Ver Información"  onclick="show('{{ $asignado->id }}','{{ $asignado->cedula }}','{{ $asignado->nombre }}','{{ $asignado->direccion }}','{{ $asignado->phone }}','{{ $asignado->cdg_viaje }}','{{ $asignado->dsc_origen}}','{{ $asignado->dsc_destino}}','{{ $asignado->costo_viaje}}','{{ $asignado->nro_plazas}}')"><i class="fa fa-search"></i></button>

                            <button class="btn btn-primary btn-sm"  title="Editar Viajero" onclick="llenar('{{ $asignado->id }}','{{ $asignado->travel_id }}','{{ $asignado->traveler_id }}')"><i class="fa fa-edit"></i></button>
                            <button class="btn btn-danger btn-sm" title="Anular Viajero" onclick="deleteV('{{ $asignado->id }}','{{ $asignado->cedula }}','{{ $asignado->nombre }}','{{ $asignado->direccion }}','{{ $asignado->phone }}')"><i class="fa fa-trash"></i></button>

                        </center>
                               
                           
                            </td>
                          
                        </tr>
                        @endforeach
                    </tbody>

                   
                </table>

                <hr>
                <center>

                {{ $asignados->links()}}
                   </center>
                </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

<!--Inclusion de modal Para Nuevo Viaje-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ingreso | Edición Viajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="alert alert-danger" role="alert" id="erros" style="display: none">
                 <strong><i class="fa fa-info-circle"> </i> Error !!</strong>  <a href="#" class="alert-link"></a>.
               </div>

        <form  >
  <div class="form-group">
    <label for="exampleInputEmail1">Viajero</label>
    <select class="form-control" id="viajero">
      <option value="">-Seleccionar-</option>
        @foreach($viajeros as $viajero)
            <option value="{{ $viajero->id }}">({{ $viajero->cedula}}) {{ $viajero->nombre }}</option>


        @endforeach
      
    </select>
    
  
  </div>
 

  <div class="form-group">
    <label for="exampleInputPassword1">Viajes Disponibles </label>
    <select class="form-control" id="viaje_disp">
      <option value="">-Seleccionar-</option>
        @foreach($viajes as $viaje)
            <option value="{{ $viaje->id }}">({{ $viaje->cdg_viaje }}) {{ $viaje->dsc_origen }} - {{ $viaje->dsc_destino }}</option>


        @endforeach
      
    </select>
    <input type="hidden" id="id_asignado">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </div>

 

</form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_new" class="btn btn-primary" onclick="newV()"><i class="fa fa-check-circle"></i> Guardar </button>
          <button type="button" id="btn_edit" class="btn btn-primary" onclick="UpdateV()"><i class="fa fa-pencil"></i> Guardar Cambios </button>
      
      </div>
    </div>
  </div>
</div>

<!--Ver detalle de viaje-->

<div class="modal fade" id="showViaje" tabindex="-1" role="dialog" aria-labelledby="showViaje" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="showViaje">Detalle De Viaje Reservado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="Mdet">
          
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    
      
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript">
 
//establecemos permisos de seguridad para el enlace con ajax y controlador 

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

/*Limpar Formulario*/
    function limpiar(){

   $('#viajero').val("");
   $('#viaje_disp').val("");
//inabilitar botones a la hora de nuevo registro
   $('#btn_edit').hide();
   $('#btn_new').show();

             
    }


     function newV(){

  $.ajax({
   url:'asignar'
  ,type:'post'
  ,data:{//data relacionada a formulario de registro
 '_token': $('input[name=_token]').val(),
  'viajero':$('#viajero').val(),
  'viajes':$('#viaje_disp').val(), 
  

}
  ,success: function(data){
    if ((data.status===500)) {
            //verificamos que campos seran validados
             errorsHtml="";
           $.each(data.errors, function (key, value) {
             errorsHtml += value +"\n";
          });

              //validamos que no sea un error en bases de datos
       $("#erros").show();
          $('.alert-link').text(errorsHtml);
           setTimeout(function() { $("#erros").hide(); },8000);

           
    }else{
         //limpiamos en formulario y ocultamos modals
            limpiar();/*Limpiamos formulario*/           
        
         $('#exampleModal').modal('hide');  //ocultamos formulario  
          setTimeout(function() { $("#success").fadeIn(1500);},1500);//activamos Notificación
          $('.alert-link').text("Su Solicitud Se Ejecuto Satisfactoriamente");//mensaje de notificación
           setTimeout(function() { $("#success").fadeOut(1500); },6000);//desactivamos notidicación        

             setTimeout("location.href='asignar'",4000);    
    }
    
    }
  ,error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.responseText);
        alert(thrownError);
  }
  });
   
   }


   //mostrar detalle del viaje
function show(id,cedula,nombre,direccion,phone,cdg,origen,destino,costo,plaza){

$('#Mdet').html('<table class="table table-striped"><tr><th>ID:<th><td>'+id+'</td></tr><tr><th>Cédula:<th><td>'+cedula+'</td></tr><tr><th>Nombre y Apellido:<th><td>'+nombre+'</td></tr><tr><th>Direccion:<th><td>'+direccion+'</td></tr><tr><th>Telefono:<th><td>'+phone+'</td></tr><tr><th>Código de Viaje:<th><td>'+cdg+'</td></tr><tr><th>Nro de plazas :<th><td>'+ plaza +'</td></tr><tr><th>Origen:<th><td>'+origen+'</td></tr><tr><th>Destino:<th><td>'+destino+'</td></tr><tr><th>Costo:<th><td>'+costo+'</td></tr></table>');
$('#showViaje').modal('show');
}


//editar viajero
   function UpdateV(){
   
    var id=$('#id_asignado').val();
 
  $.ajax({
   url:'asignar/'+ id
  ,type:'PUT'
  ,data:{//data relacionada a formulario de registro
  '_token': $('input[name=_token]').val(),
  'viajero':$('#viajero').val(),
  'viajes':$('#viaje_disp').val(), 

}
  ,success: function(data){
    if ((data.status===500)) {

          //verificamos que campos seran validados
             errorsHtml="";
           $.each(data.errors, function (key, value) {
             errorsHtml += value +"\n";
          });

              //validamos que no sea un error en bases de datos
       $("#erros").show();
          $('.alert-link').text(errorsHtml);
           setTimeout(function() { $("#erros").hide(); },8000);
           
    }else{
            //limpiamos en formulario y ocultamos modals
            limpiar();/*Limpiamos formulario*/         
        
         $('#exampleModal').modal('hide');  //ocultamos formulario
          setTimeout(function() { $("#success").fadeIn(1500);},3000);//activamos Notificción
          $('.alert-link').text("La Modificación  Se Ejecuto Satisfactoriamente");
                    //mensaje de notificación 
                     setTimeout(function() { $("#success").fadeOut(1500); },6000);//desactivamos notificación
                     setTimeout("location.href='asignar'",4000);  

    }
    
    }
  ,error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.responseText);
        alert(thrownError);
  }
  });
   
   }

    //Cargar Formulario para editar //
function llenar(id,id_viajes,id_viajero){

   $('#exampleModal').modal('show');
   $('#id_asignado').val(id);
   $('#viajero').val(id_viajero);
   $('#viaje_disp').val(id_viajes);
   //habilito boton guardar cambios
   $('#btn_edit').show();
   $('#btn_new').hide();


}



//eliminamos asignación del viajero en algún viaje
    function deleteV(id){

        //mensaje de validación de accion delete//
    if(confirm("Seguro que desea Eliminar El Viaje Reservado?")){
    $.ajax({    type: 'DELETE',//metoodo 
                url: 'asignar/' + id,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                  
                 setTimeout("location.href='asignar'",1500);     
                }
            });
}
}

</script>



