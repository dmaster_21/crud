@extends('layouts.app')

@section('content')

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><b> <h1 class="text-primary">CRUD de Viajes</h1></b></center></div>

                <div class="card-body">

                    <center>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="limpiar()"><i class="fa fa-plus"></i> Crear Viajes</button>
                    </center>
                <hr>
                <!--Mensajes de alertas-->
                <div class="alert alert-success" role="alert"  id="success" style="display: none">
                 <strong><i class="fa fa-check-circle"> </i> Exito !!</strong>  <a href="#" class="alert-link">Viaje Generado de Manera Exitosa!</a>.
               </div>
                
                



                <table class="table table-striped" id="tabviajes">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Código De Viaje</th>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Costo</th>
                            <th></th>
                        </tr>
                    </thead>
    

                    <tbody>
                        @foreach($viajes as $viaje)
                        <tr id="tab{{ $viaje->id }}">
                            <td>{{ $viaje->id }}</td>
                            <td>{{ $viaje->cdg_viaje }}</td>
                            <td>{{ $viaje->dsc_origen }}</td>
                            <td>{{ $viaje->dsc_destino }}</td>
                            <td><span class="badge badge-dark"> $.{{ $viaje->costo_viaje }}</span></td>
                            <td>
                                <center>
                            <button class="btn btn-success btn-sm"  title="Ver Información"  onclick="show('{{ $viaje->id }}','{{ $viaje->cdg_viaje }}','{{ $viaje->nro_plazas }}','{{ $viaje->dsc_origen }}','{{ $viaje->dsc_destino }}','{{ $viaje->costo_viaje }}')"><i class="fa fa-search"></i></button>
                            <button class="btn btn-primary btn-sm" title="Editar Viajes" onclick="llenar('{{ $viaje->id }}','{{ $viaje->cdg_viaje }}','{{ $viaje->nro_plazas }}','{{ $viaje->dsc_origen }}','{{ $viaje->dsc_destino }}','{{ $viaje->costo_viaje }}')"><i class="fa fa-edit"></i></button>
                            <button class="btn btn-danger btn-sm" title="Anular Viajes" onclick="deleteV('{{ $viaje->id }}','{{ $viaje->dsc_destino }}','{{ $viaje->dsc_origen }}')"><i class="fa fa-trash"></i></button>

                        </center>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                   
                </table>

                <hr>
                <center>

                {{ $viajes->links()}}
                   </center>
                </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

<!--Inclusion de modal Para Nuevo Viaje-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ingreso | Edición Viajes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="alert alert-danger" role="alert" id="erros" style="display: none">
                 <strong><i class="fa fa-info-circle"> </i> Error !!</strong>  <a href="#" class="alert-link">Contacte con el Administrador del Sistema</a>.
               </div>

        <form  >
  <div class="form-group">
    <label for="exampleInputEmail1">Código De Viaje</label>
    <input type="text" id="codg_viaje" class="form-control text-uppercase" >
    <input type="hidden" id="id_viaje" >
  
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Número de Plazas </label>
    <input type="number" id="nro_pza" class="form-control" >
   
  </div>

   <div class="form-group">
    <label for="exampleInputPassword1">Origen </label>
    <input type="text" id="origen" class="form-control text-uppercase" >
    
  </div>

   <div class="form-group">
    <label for="exampleInputPassword1">Destino</label>
    <input type="text" id="destino" class="form-control text-uppercase" >
    
  </div>

 <div class="form-group">
    <label for="exampleInputPassword1">Costo </label>
    <input type="number" id="costo" class="form-control" >
   

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </div>

</form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_new" class="btn btn-primary" onclick="newV()"><i class="fa fa-check-circle"></i> Guardar </button>
          <button type="button" id="btn_edit" class="btn btn-primary" onclick="UpdateV()"><i class="fa fa-pencil"></i> Guardar Cambios </button>
      
      </div>
    </div>
  </div>
</div>

<!--Ver detalle de viaje-->

<div class="modal fade" id="showViaje" tabindex="-1" role="dialog" aria-labelledby="showViaje" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="showViaje">Detalle De Viaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="Mdet">
          
        </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    
      
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript">
 
//establecemos permisos de seguridad para el enlace con ajax y controlador 

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});



//Limpar Formulario
    function limpiar(){

   $('#codg_viaje').val("");
   
   $('#nro_pza').val("");
   $('#origen').val("");
   $('#destino').val("");
   $('#costo').val("");

//inabilitar botones a la hora de nuevo registro
   $('#btn_edit').hide();
   $('#btn_new').show();

     
        
    }
   //funcion para crear un nuevo registro de viaje 
   function newV(){

  $.ajax({
   url:'viajes'
  ,type:'post'
  ,data:{//data relacionada a formulario de registro
  '_token': $('input[name=_token]').val(),
  'codigo_viaje':$('#codg_viaje').val(),
  'nro_plazas':$('#nro_pza').val(),
  'origen':$('#origen').val(),
  'destino':$('#destino').val(),
  'costo':$('#costo').val()

}
  ,success: function(data){
    if ((data.status===500)) {
            //verificamos que campos seran validados
             errorsHtml="";
           $.each(data.errors, function (key, value) {
             errorsHtml += value +"\n";
          });

              //validamos que no sea un error en bases de datos
       $("#erros").show();
          $('.alert-link').text(errorsHtml);
           setTimeout(function() { $("#erros").hide(); },8000);

           
    }else{
         //limpiamos en formulario y ocultamos modals
            limpiar();/*Limpiamos formulario*/           
        
         $('#exampleModal').modal('hide');  //ocultamos formulario  
          setTimeout(function() { $("#success").fadeIn(1500);},3000);//activamos Notificación
          $('.alert-link').text("La Creacion Del Viaje Se Ejecuto Satisfactoriamente");//mensaje de notificación
           setTimeout(function() { $("#success").fadeOut(1500); },6000);//desactivamos notidicación
           setTimeout("location.href='viajes'",4000);  //recargamos vista 

        
    }
    
    }
  ,error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.responseText);
        alert(thrownError);
  }
  });
   
   }
//mostrar detalle del viaje
function show(id,cdg_viaje,nro_plaza,desc_origen,dsc_destino,costo){

$('#Mdet').html('<table class="table table-striped"><tr><th>ID:<th><td>'+id+'</td></tr><tr><th>Código de Viaje:<th><td>'+cdg_viaje+'</td></tr><tr><th>Número de Plazas:<th><td>'+nro_plaza+'</td></tr><tr><th>Origen:<th><td>'+desc_origen+'</td></tr><tr><th>Destino:<th><td>'+dsc_destino+'</td></tr><tr><th>Costo:<th><td>$'+costo+'</td></tr></table>');
$('#showViaje').modal('show');
}

//Cargar Formulario para editar //
function llenar(id,cdg_viaje,nro_plaza,desc_origen,dsc_destino,costo){
   $('#exampleModal').modal('show');
   $('#id_viaje').val(id);
   $('#codg_viaje').val(cdg_viaje);
   $('#nro_pza').val(nro_plaza);
   $('#origen').val(desc_origen);
   $('#destino').val(dsc_destino);
   $('#costo').val(costo);

   //habilito boton guardar cambios
   $('#btn_edit').show();
   $('#btn_new').hide();


}

/**actualizamos información del viaje*/
//funcion para crear un nuevo registro de viaje 
   function UpdateV(){
    var id=$('#id_viaje').val()
  $.ajax({
   url:'viajes/'+id
  ,type:'PUT'
  ,data:{//data relacionada a formulario de registro
  '_token': $('input[name=_token]').val(),
  'codigo_viaje':$('#codg_viaje').val(),
  'nro_plazas':$('#nro_pza').val(),
  'origen':$('#origen').val(),
  'destino':$('#destino').val(),
  'costo':$('#costo').val()

}
  ,success: function(data){
    if ((data.status===500)) {

          //verificamos que campos seran validados
             errorsHtml="";
           $.each(data.errors, function (key, value) {
             errorsHtml += value +"\n";
          });

              //validamos que no sea un error en bases de datos
       $("#erros").show();
          $('.alert-link').text(errorsHtml);
           setTimeout(function() { $("#erros").hide(); },8000);
           
    }else{

            //limpiamos en formulario y ocultamos modals
            limpiar();/*Limpiamos formulario*/           
        
         $('#exampleModal').modal('hide');  //ocultamos formulario
          setTimeout(function() { $("#success").fadeIn(1500);},3000);//activamos Notificción
          $('.alert-link').text("La Modificación Del Viaje Se Ejecuto Satisfactoriamente");//mensaje de notificación
           setTimeout(function() { $("#success").fadeOut(1500); },6000);//desactivamos notificación
           setTimeout("location.href='viajes'",4000);  


    }
    
    }
  ,error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.responseText);
        alert(thrownError);
  }
  });
   
   }
/*Delete viajes*/
function deleteV(id,destino,origen){


    //mensaje de validación de accion delete//
    if(confirm("Seguro que desea Eliminar El Viaje Con Origen a "+origen+" y Destino a :"+destino+"?")){

    $.ajax({
                type: 'DELETE',//metoodo 
                url: 'viajes/' + id,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                  
                   setTimeout("location.href='viajes'",1500);   //reinicio busqueda             
                }
            });
}
}








</script>



