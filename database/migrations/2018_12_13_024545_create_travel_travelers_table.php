<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelTravelersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_travelers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('travel_id');
            $table->foreign('travel_id')->references('id')->on('travels');         
            $table->unsignedInteger('traveler_id');
            $table->foreign('traveler_id')->references('id')->on('travelers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_travelers');
    }
}
