<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traveler extends Model
{
	  protected $fillable = ['cedula', 'nombre','direccion','phone'];


   public function travel_travelers(){

    	return $this->belongsToMany(Travel_travelers::class);
    }
    
}
