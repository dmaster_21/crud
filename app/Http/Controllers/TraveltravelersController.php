<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Travel;
use App\Traveler;
use App\Travel_traveler;
class TraveltravelersController extends Controller
{
    //

    public function index()
    {
      $travel =Travel::all();
      $traveler =Traveler::all();
      $Asignar=DB::table('travel_travelers')
      ->Join('travels', 'travel_travelers.travel_id', '=', 'travels.id')
      ->Join('travelers', 'travel_travelers.traveler_id', '=', 'travelers.id')->select('travel_travelers.id','travel_travelers.travel_id','travel_travelers.traveler_id','travelers.cedula','travelers.nombre','travelers.direccion','travelers.phone','travels.cdg_viaje','travels.dsc_origen','travels.dsc_destino','travels.costo_viaje','travels.nro_plazas')->paginate(15);
    
      return view('asignados.index')->with(['asignados'=>$Asignar,'viajes'=>$travel,'viajeros'=>$traveler]);
    
    }



public function store(Request $request)
    {
   
$validation = Validator::make($request->all(), [
        'viajero' => 'required',
        'viajes' => 'required',
        

   ]);
  

  if(count($validation->errors()) > 0){
            return response()->json(['status' => 500,'errors' => $validation->errors()]);
      }else{
  		//Creamos nueva Relación
       $Traveled =new Travel_traveler();
       $Traveled->travel_id=$request->viajes;
       $Traveled->traveler_id=$request->viajero ;  
                   
       $Traveled->save();
      
      return  Response()->json($Traveled);


 	}
    
    }



    public function update(Request $request, $id)
    {


    	 //verificamos que los campos cumplan con los parametros
  
   
$validation = Validator::make($request->all(), [
        'viajero' => 'required',
        'viajes' => 'required',
        

   ]);
  

  if(count($validation->errors()) > 0){
            return response()->json(['status' => 500,'errors' => $validation->errors()]);
      }else{
  		
       $Traveled =Travel_traveler::find($id);
       $Traveled->travel_id=$request->viajes;     
       $Traveled->traveler_id=$request->viajero;
                   
       $Traveled->save();
      
        return  Response()->json($Traveled);
    

 	}
    
      

    }




   
  /*eliminamos el registro que guarda relacion con el viajero y el viaje*/
   public function destroy($id)
    {
      $Traveled =Travel_traveler::find($id);
      $Traveled ->delete();
    }

}

