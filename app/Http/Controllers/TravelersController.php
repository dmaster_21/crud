<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Traveler;
class TravelersController extends Controller
{
    
	  public function index()
    {

       //consultamos los registros de viajes creados paginados
        $Travel= DB::table('travelers')->paginate(10);
        //devolvemos la vista con el resultado
    	return view('viajeros.index')->with('viajeros',$Travel);
    }

     public function store(Request $request)
    {
   
$validation = Validator::make($request->all(), [
        'cedula' => 'required|min:7|max:8|regex:/^[0-9 ,.\'-]+$/i',
        'nombres' => 'required',
        'direccion' => 'required',
        'telefono' => 'required|min:11|max:11|regex:/^[0-9 ,.\'-]+$/i',
        


   ]);
  

  if(count($validation->errors()) > 0){
            return response()->json(['status' => 500,'errors' => $validation->errors()]);
      }else{
  		//verificamos si ya existe el viajero
         $BTravel= Traveler::where('cedula',$request->cedula)->first();
         if($BTravel){
           return response()->json(['status' => 404,'errors' => 'La Cédula del Viajero Ya Existe.']);
         }else{

         
        //
       $Travel =new Traveler();//creamos nuevo objeto
       $Travel->cedula=$request->cedula ;     
       $Travel->nombre=$request->nombres;
       $Travel->direccion=$request->direccion;
       $Travel->phone=$request->telefono;
            
       $Travel->save();
      // return $Tview;
      return  Response()->json($Travel);

    }
 	}
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*
    public function show($id)
    {
        //
    }
*/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    	$validation = Validator::make($request->all(), [
        'cedula' => 'required|min:7|max:8|regex:/^[0-9 ,.\'-]+$/i',
        'nombres' => 'required',
        'direccion' => 'required',
        'telefono' => 'required|min:11|max:11|regex:/^[0-9 ,.\'-]+$/i',
        


   ]);
  

  if(count($validation->errors()) > 0){
            return response()->json(['status' => 500,'errors' => $validation->errors()]);
      }else{
  		//ingresod e nuevo registro
       $Travel =Traveler::find($id);//Ejecuto Busqueda del viaje mediante el ID        
       $Travel->nombre=$request->nombres;
       $Travel->direccion=$request->direccion;
       $Travel->phone=$request->telefono;
            
       $Travel->save();
     
      return  Response()->json($Travel);


 	
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $Travel =Traveler::find($id);
      $Travel ->delete();
    }


     public function show($id){

      $Ptraveler = Traveler::find($id);
      return  Response()->json($Ptraveler);
    }

}
