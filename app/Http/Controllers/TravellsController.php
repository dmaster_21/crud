<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request ;
use Validator;
use App\Travel;

class TravellsController extends Controller
{




	  public function index()
    {

       //consultamos los registros de viajes creados paginados
        $Tviaje= DB::table('travels')->paginate(10);
        //devolvemos la vista con el resultado
    	return view('viajes.index')->with('viajes',$Tviaje);
    }

     public function store(Request $request)
    {
   //verificamos que los campos cumplan con los parametros
  
$validation = Validator::make($request->all(), [
        'codigo_viaje' => 'required|min:5|max:100',
        'nro_plazas' => 'required|min:1|max:5|regex:/^[0-9 ,.\'-]+$/i',
        'destino' => 'required|min:1|max:100',
        'origen' => 'required|min:1|max:100',
        'costo' =>'required|min:1|max:15|regex:/^[0-9 ,.\'-]+$/i'


   ]);
  

  if(count($validation->errors()) > 0){
            return response()->json(['status' => 500,'errors' => $validation->errors()]);
      }else{
       
  		//ingresod e nuevo registro
       $Tview =new Travel();//creamos nuevo objeto
       $Tview->cdg_viaje=$request->codigo_viaje;       
       $Tview->nro_plazas=$request->nro_plazas;
       $Tview->dsc_destino=$request->destino;
       $Tview->dsc_origen=$request->origen;
       $Tview->costo_viaje=$request->costo;      
       $Tview->save();
      // return $Tview;
      return  Response()->json($Tview);


 	}
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*
    public function show($id)
    {
        //
    }
*/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    	 //verificamos que los campos cumplan con los parametros
  
$validation = Validator::make($request->all(), [
        'codigo_viaje' => 'required|min:5|max:100',
        'nro_plazas' => 'required|min:1|max:5|regex:/^[0-9 ,.\'-]+$/i',
        'destino' => 'required|min:1|max:100',
        'origen' => 'required|min:1|max:100',
        'costo' =>'required|min:1|max:15|regex:/^[0-9 ,.\'-]+$/i'


   ]);
  

  if(count($validation->errors()) > 0){
            return response()->json(['status' => 500,'errors' => $validation->errors()]);
      }else{
  		
       $Tview =Travel::find($id);//Ejecuto Busqueda del viaje mediante el ID
       $Tview->cdg_viaje=$request->codigo_viaje;       
       $Tview->nro_plazas=$request->nro_plazas;
       $Tview->dsc_destino=$request->destino;
       $Tview->dsc_origen=$request->origen;
       $Tview->costo_viaje=$request->costo;      
       $Tview->save();
       //return $Tview;
    return  Response()->json($Tview);

 	}
    
      

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $Tview =Travel::find($id);
      $Tview ->delete();
    }



    public function viajes($id){

      $Ptravel = Travel::find($id);
      return $Ptravel;
    }
}
