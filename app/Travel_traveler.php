<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel_traveler extends Model
{
    //
     //
    public function travels(){
//relacion muhchos a muchos 
    	return $this->belongsToMany(Travel::class);
    }

     public function travelers(){
//relacion muhchos a muchos 
    	return $this->belongsToMany(Traveler::class);
    }
}
