<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{

	protected $fillable = ['cdg_viaje', 'nro_plazas','dsc_destino','dsc_origen','costo_viaje'];

   public function travel_travelers(){

    	return $this->belongsToMany(Travel_traveler::class);
    }
}
