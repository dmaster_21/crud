# CRUD CON LARAVEL

Indicaciones de Instalacion DEMO-CRUD

## Instalacion

Clonar Repositorio
```
git clone https://gitlab.com/dmaster_21/crud.git
```

ejecutar Manejador de dependencias
```
composer install | composer update
```
ejecutar migraciones establecidas
```
php artisan migrate
```

Copiamos y reemplazamos el archivo .env.example de modo que generemos el archivo .env

Definimos parametros de conexion de Base de datos dentro del archivo .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Finalmente Generar Clave laravel
```
php artisan key:generate
```

Especificaciones del Demo:

## Viajeros
1. Crear Viajero
2. Ver Infromación de Viajero
3. Editar El Viajero
4. Eliminar Viajero


## Viajes
1. Crear Viaje
2. Ver Información de Viaje
3. Editar Viaje
4. Eliminar Viaje


## Reservacion
1. Asignar viaje a Viajero
2. Ver Detalle del vieje y viajero
3. Editar Reservacion| Relación
4. Eliminar Reservación



