<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/viajes', 'TravellsController@index')->name('viajes');

Route::apiResource('viajes','TravellsController');
Route::apiResource('viajeros','TravelersController');
Route::apiResource('asignar','TraveltravelersController');